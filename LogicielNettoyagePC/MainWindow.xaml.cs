﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LogicielNettoyagePC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string urlActualite = "http://localhost/LogicielNettoyagePC/actu.txt";
        public DirectoryInfo winTemp;
        public DirectoryInfo AppTemp;

        public MainWindow()
        {
            InitializeComponent();
            winTemp = new DirectoryInfo(@"C:\Windows\Temp");
            AppTemp = new DirectoryInfo(System.IO.Path.GetTempPath());
            LastAnalyseDate();

            CheckActu();
        }
        /// <summary>
        /// Vérifier les actus
        /// </summary>
        public void CheckActu()
        {
            using (WebClient client= new WebClient())
            {
                string actualite = client.DownloadString(urlActualite);
                if(actualite != String.Empty)
                {
                    actu.Visibility = Visibility.Visible;
                    actu.Content = actualite;
                }
            }
        }
        /// <summary>
        /// Calcul de la taille des dossiers
        /// </summary>
        /// <param name="dir">Dossier à traiter</param>
        /// <returns>Retourne la taille des dossiers + fichier</returns>
        private long FolderSize(DirectoryInfo dir)
        {
            return dir.GetFiles().Sum(fi => fi.Length) + dir.GetDirectories().Sum(di => FolderSize(di));
        }
        /// <summary>
        /// Suprimmer le dossier temporaire de windows
        /// </summary>
        /// <param name="di">Liste des dossier à supprimer</param>
        public void ClearTempData(DirectoryInfo di)
        {
            foreach(FileInfo file in di.GetFiles())
            {
                try
                {
                    file.Delete();
                    Console.WriteLine(file.FullName);
                }
                catch(Exception ex)
                {
                    continue;
                }
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                try
                {
                    dir.Delete();
                    Console.WriteLine(dir.FullName);
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
        }
        /// <summary>
        /// Analyse du pc à nettoyer
        /// </summary>
        public void AnalyseFolders()
        {
            Console.WriteLine("Début de l'analyse...");
            long totalSize = 0;
            
            try
            {
                totalSize += FolderSize(winTemp) / 1000000;
                totalSize += FolderSize(AppTemp) / 1000000;
                espace.Content = totalSize + " Mb";
                titre.Content = "Analyse effectuée";
                date.Content = DateTime.Today;
                SaveDate();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        private void Button_Maj_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Votre logiciel est à jour ! ","Mise à jour",MessageBoxButton.OK,MessageBoxImage.Information);
        }

        private void Button_Historique_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("L'historique n'est pas encore implémenté ! ", "Historique", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void Button_Web_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(new ProcessStartInfo("https://www.google.com")
                {
                    UseShellExecute = true
                });
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }

        private void Button_Analyser_Click(object sender, RoutedEventArgs e)
        {
            AnalyseFolders();
        }

        private void Button_Nettoyer_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Netoyage en cours...");
            btn_Nettoyer.Content = "NETTOYAGE EN COURS";

            Clipboard.Clear();
            try
            {
                ClearTempData(winTemp);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            btn_Nettoyer.Content = "NETTOYAGE \n TERMINE";
            titre.Content = "Nettoyage effectué!";
            espace.Content = "0 Mb";
        }
        /// <summary>
        /// Sauvegarder la date lors d'une analyse
        /// </summary>
        public void SaveDate()
        {
            string date = DateTime.Today.ToString();
            File.WriteAllText("date.txt", date);
        }
        public void LastAnalyseDate()
        {
            string lastDate = File.ReadAllText("date.txt");

            if(lastDate != String.Empty)
            {
                date.Content = lastDate;
            }
        }
    }
}
